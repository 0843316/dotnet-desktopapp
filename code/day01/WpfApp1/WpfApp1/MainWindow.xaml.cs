﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {      
        public MainWindow()
        {
            InitializeComponent();
            List<Person> people = new List<Person>();
            people.Add(new Person { FirstName = "Johnny", LastName = "Fougee" });
            people.Add(new Person { FirstName = "Albert", LastName = "Jacob" });
            people.Add(new Person { FirstName = "Patrick", LastName = "Fougee" });
            people.Add(new Person { FirstName = "Johnny", LastName = "Abee" });
            people.Add(new Person { FirstName = "El Fouenzi", LastName = "Md" });
            cbxPeople.ItemsSource = people;
        }

        private void btnMe_Click(object sender, RoutedEventArgs e)
        {
            txtblkTitle.Text += ": " + txtName.Text ;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(txtName.Text);
        }

        private void cbxPeople_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
