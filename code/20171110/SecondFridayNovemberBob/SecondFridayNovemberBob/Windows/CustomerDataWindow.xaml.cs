﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;

namespace SecondFridayNovemberBob
{
    /// <summary>
    /// Interaction logic for CustomerDataWindow.xaml
    /// </summary>
    public partial class CustomerDataWindow : Window
    {
        // Has a list of Customer object as a member variable lstCustomers
        List<Customer> customers = new List<Customer>();
        public CustomerDataWindow()
        {
            InitializeComponent();
        }
        private void LoadCustomers()
        {
            // Load the grid in the UI with info from Customer Objects in member variable lstCustomers
        }
        // TEST method
        private void TestLoadCustomer()
        {
            // Create a single Customer object            
            Customer cx = new Customer { FirstName = "J-P", LastName = "Francis" };
            customers.Add(cx);
            dgCustomers.ItemsSource = customers;
            // Load the grid with that object
        }
        private void SaveCustomers()
        {
            // save the Customer Objects' properties to a CSV file
            string csvcontent = "";
            string csvFileLocation = AppDomain.CurrentDomain.BaseDirectory + "/Files/Customers.csv";
            StreamWriter writer = new StreamWriter(csvFileLocation);            
            foreach(Customer x in customers)
            {
                csvcontent += $"{x.FirstName},{x.LastName}\n";
                writer.WriteLine(csvcontent);
            }
            writer.Close();
        }
        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            LoadCustomers();
            TestLoadCustomer(); // for learning purposes only
        }
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            SaveCustomers();
        }
    }
}