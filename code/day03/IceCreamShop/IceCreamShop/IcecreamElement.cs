﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IceCreamShop
{
    class IcecreamElement
    {
        public string mName { get; set; } //m stands for member
        public double mPrice { get; set; } //m stands for member
        public string mCategory { get; set; } //m stands for member

        public IcecreamElement(string pName, double pPrice, string pCategory)
        {
            mPrice = pPrice;
            mName = pName;
            mCategory = pCategory;
        }
    }
}