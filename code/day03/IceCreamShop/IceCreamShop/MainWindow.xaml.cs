﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IceCreamShop
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<IcecreamElement> elements = new List<IcecreamElement>();
        private double SizesCost;
        private double FlavourCost;
        private double ToppingsCost;

        public MainWindow()
        {
            InitializeComponent();
            AddElements();

        }

        // Storing options of ice cream elements in the array elements 
        private void AddElements()
        {
            //        Small ($1.00),Medium ($2.00),Large ($3.00)
            //        Chocolate ($0.50),Vanilla ($1.00),Strawberry ($1.50)
            //        Sprinkles ($2.50),Smarties ($1.50),Nuts ($1.00),Oreo ($0.50)

            elements.Add(new IcecreamElement("Small", 1, "Size"));
            elements.Add(new IcecreamElement("Medium", 2, "Size"));
            elements.Add(new IcecreamElement("Large", 3, "Size"));
            elements.Add(new IcecreamElement("Chocolate", 0.5, "Flavour"));
            elements.Add(new IcecreamElement("Vanilla", 1, "Flavour"));
            elements.Add(new IcecreamElement("Strawberry", 1.5, "Flavour"));
            elements.Add(new IcecreamElement("Sprinkles", 2.5, "Toppings"));
            elements.Add(new IcecreamElement("Smarties", 1.5, "Toppings"));
            elements.Add(new IcecreamElement("Nuts", 1, "Toppings"));
            elements.Add(new IcecreamElement("Oreo", 0.5, "Toppings"));
        }

        // gets the name of the item the user checks...
        // for example "Small","Vanilla", "Nuts", and "Oreo"
        private string GetElementName(object sender)
        {
            string strSmall = "";
            if (sender is RadioButton)
            {


                RadioButton small = (RadioButton)sender;
                strSmall = small.Content.ToString();
                strSmall = strSmall.Split(' ')[0];
            }
            else if (sender is CheckBox)
            {
                CheckBox small = (CheckBox)sender;
                strSmall = small.Content.ToString();
                strSmall = strSmall.Split(' ')[0];
            }

            return strSmall;
        }

        // Adds the cost of the Size, Flavour and Toppings
        // Displays the cost of the icecream on the window
        private void DisplayTotal(object sender, RoutedEventArgs e)
        {
            SetPrice(sender, e);
            double price = SizesCost + FlavourCost + ToppingsCost;
            txtbxTotal.Text = price.ToString("C");
        }

        // looks at what are the Ice Cream choices
        // sets the price of the SizesCost FlavourCost and ToppingsCost;
        private void SetPrice(object sender, RoutedEventArgs e)
        {
            IcecreamElement iceCreamElement = elements.Find(element => element.mName == GetElementName(sender));
            string category = iceCreamElement.mCategory;
            switch (category)
            {
                case "Size":
                    SizesCost = iceCreamElement.mPrice;
                    break;
                case "Flavour":
                    FlavourCost = iceCreamElement.mPrice;
                    break;
                case "Toppings":
                    String eventName = e.RoutedEvent.Name;
                    switch (eventName)
                    {
                        case "Checked":
                            ToppingsCost += iceCreamElement.mPrice;
                            break;
                        case "Unchecked":
                            ToppingsCost -= iceCreamElement.mPrice;
                            break;
                    }

                    break;
            }


        }
        
        // All event handlers will call the method DisplayTotal
        private void rbtnSmall_Checked(object sender, RoutedEventArgs e)
        {
            DisplayTotal(sender, e);

        }
        private void rbtnMedium_Checked(object sender, RoutedEventArgs e)
        {
            DisplayTotal(sender, e);
        }
        private void rbtnLarge_Checked(object sender, RoutedEventArgs e)
        {
            DisplayTotal(sender, e);
        }
        private void chkbxOreo_Checked(object sender, RoutedEventArgs e)
        {
            DisplayTotal(sender, e);
        }
        private void chkbxNuts_Checked(object sender, RoutedEventArgs e)
        {
            DisplayTotal(sender, e);
        }
        private void chkbxSmarties_Checked(object sender, RoutedEventArgs e)
        {
            DisplayTotal(sender, e);
        }
        private void chkbxSprinkles_Checked(object sender, RoutedEventArgs e)
        {
            DisplayTotal(sender, e);
        }
        private void rbtnStrawberry_Checked(object sender, RoutedEventArgs e)
        {
            DisplayTotal(sender, e);
        }
        private void rbtnVanilla_Checked(object sender, RoutedEventArgs e)
        {
            DisplayTotal(sender, e);

        }
        private void rbtnChocolate_Checked(object sender, RoutedEventArgs e)
        {
            DisplayTotal(sender, e);
        }
        private void chkbxSprinkles_Unchecked(object sender, RoutedEventArgs e)
        {
            DisplayTotal(sender, e);
        }
        private void chkbxSmarties_Unchecked(object sender, RoutedEventArgs e)
        {
            DisplayTotal(sender, e);
        }
        private void chkbxNuts_Unchecked(object sender, RoutedEventArgs e)
        {
            DisplayTotal(sender, e);
        }
        private void chkbxOreo_Unchecked(object sender, RoutedEventArgs e)
        {
            DisplayTotal(sender, e);
        }
    }
}
