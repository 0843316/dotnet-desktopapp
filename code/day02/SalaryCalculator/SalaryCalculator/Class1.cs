﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalaryCalculator
{
    class Calculator
    {
        public double mWage { get; set; }
        public int mHours { get; set; }


        public Calculator()
        {
            mWage = 0;
            mHours = 0;
        }

        Calculator(double pWage, int pHours)
        {
            mWage = pWage;
            mHours = pHours;
        }
        public double Calculate()

                //            <TextBlock Text = "If salary >= 1600, tax is 40%" ></ TextBlock >
                //< TextBlock Text="If salary is between 1200-1600, tax is 30%"></TextBlock>
                //<TextBlock Text = "If salary is between 900-1200, tax is 20%" ></ TextBlock >
                //< TextBlock Text="If salary is less than 900 tax is 10%"></TextBlock>

        {
            
            double salary = mWage * mHours;
            if (salary >= 1600)
            {
                salary *= 0.6;
            } else if (salary >= 1200)
            {
                salary *= 0.7;
            } else if (salary >= 900)
            {
                salary *= 0.8;
            } else if (salary < 900)
            {
                salary *= 0.8;
            }
            return salary ;
        }
    }
}
