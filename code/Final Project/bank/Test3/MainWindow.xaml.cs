﻿using System;
using System.Windows;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.IO;

namespace Test3
{
    public partial class MainWindow : Window
    {
        DataTable dt = new DataTable("Users");
        Account loginAccount = new Account();

        string ConString = ConfigurationManager.ConnectionStrings["BankAccount.Properties.Settings.BankAccountsConnectionString"].ConnectionString;
        string CmdString = string.Empty;
        bool isLoggedIn = false;

        public MainWindow()
        {
            InitializeComponent();
            this.SizeToContent = SizeToContent.Height; // Automatically resize height relative to content.

        }





        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            login();
            DisplayAccount();
        }

        private void login()
        {

            LoginWindow loginWindow = new LoginWindow();
            loginWindow.ShowDialog();
            loginAccount = loginWindow.loginAccount;
            MessageBox.Show("Welcome " + loginAccount.Name + " !");
            if (loginAccount.Name.Equals("Admin"))
            {
                this.Hide();
                AdminPanel adminPanelWindow = new AdminPanel();
                adminPanelWindow.ShowDialog();
                this.Show();
            }
            else
            {
                isLoggedIn = true;
                DisplayAccount();

            }
        }

        private void DisplayAccount()
        {
            txtbxAmount.Text = "";
            if (isLoggedIn)
            {
                btnLogin.Visibility = Visibility.Hidden;
                btnLogout.Visibility = Visibility.Visible;
            }
            else
            {
                loginAccount = new Account();
                btnLogin.Visibility = Visibility.Visible;
                btnLogout.Visibility = Visibility.Hidden;
            }

            txtbxAmount.IsEnabled = isLoggedIn;
            lblName.Text = loginAccount.Name;
            lblID.Text = loginAccount.Id.ToString();
            lblBalance.Text = loginAccount.Balance.ToString("C");
            btnDeposit.IsEnabled = isLoggedIn;
            btnWithdraw.IsEnabled = isLoggedIn;
            btnLogin.IsEnabled = !isLoggedIn;
            btnLogout.IsEnabled = isLoggedIn;
        }

        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            isLoggedIn = false;
            DisplayAccount();
        }

        private void btnDeposit_Click(object sender, RoutedEventArgs e)
        {
            Deposit();
            DisplayAccount();
        }

        private void Deposit()
        {
            DataSet ds = new DataSet();
            using (SqlConnection con = new SqlConnection(ConString))
            {
                try
                {
                    CmdString = "Select A.Balance FROM Acconts AS A INNER JOIN Users AS U ON A.AccountNumber = U.AccountNumber WHERE U.AccountNumber = \'" + loginAccount.AccountNumber + "\';";
                    SqlCommand cmd = new SqlCommand(CmdString, con);
                    SqlDataAdapter sda = new SqlDataAdapter(cmd);
                    sda.Fill(ds, "Balance");
                    dt = ds.Tables["Balance"];

                    double balance = Double.Parse(dt.Rows[0]["Balance"].ToString());

                    if (loginAccount.Balance == balance)
                    {

                        loginAccount.Balance += Int32.Parse(txtbxAmount.Text);
                        CmdString = "UPDATE A SET Balance = " + loginAccount.Balance + " FROM Acconts AS A INNER JOIN Users AS U ON A.AccountNumber = U.AccountNumber	WHERE U.AccountNumber = \'" + loginAccount.AccountNumber + "\';";
                        cmd = new SqlCommand(CmdString, con);
                        sda = new SqlDataAdapter(cmd);
                        con.Open();
                        cmd.Connection = con;
                        cmd.ExecuteNonQuery();
                        con.Close();
                        DisplayAccount();
                    }
                    else
                    {
                        MessageBox.Show("Balance isn't the same as on the DB");
                    }
                }
                catch (Exception ex)
                {
                    con.Close();
                    MessageBox.Show(ex.Message);
                }
            }

        }

        private void btnWithdraw_Click(object sender, RoutedEventArgs e)
        {
            Withdraw();
            DisplayAccount();
        }
        private void Withdraw()
        {

            try
            {
                if (loginAccount.Balance >= Int32.Parse(txtbxAmount.Text))
                {
                    DataSet ds = new DataSet();
                    using (SqlConnection con = new SqlConnection(ConString))
                    {
                        try
                        {
                            CmdString = "Select A.Balance FROM Acconts AS A INNER JOIN Users AS U ON A.AccountNumber = U.AccountNumber WHERE U.AccountNumber = \'" + loginAccount.AccountNumber + "\';";
                            SqlCommand cmd = new SqlCommand(CmdString, con);
                            SqlDataAdapter sda = new SqlDataAdapter(cmd);
                            sda.Fill(ds, "Balance");
                            dt = ds.Tables["Balance"];

                            double balance = Double.Parse(dt.Rows[0]["Balance"].ToString());

                            if (loginAccount.Balance == balance)
                            {
                                loginAccount.Balance -= Int32.Parse(txtbxAmount.Text);
                                CmdString = "UPDATE A SET Balance = " + loginAccount.Balance + " FROM Acconts AS A INNER JOIN Users AS U ON A.AccountNumber = U.AccountNumber	WHERE U.AccountNumber = \'" + loginAccount.AccountNumber + "\';";
                                cmd = new SqlCommand(CmdString, con);
                                sda = new SqlDataAdapter(cmd);
                                con.Open();
                                cmd.Connection = con;
                                cmd.ExecuteNonQuery();
                                con.Close();
                                DisplayAccount();
                            }
                            else
                            {
                                MessageBox.Show("Balance isn't the same as on the DB");
                            }
                        }
                        catch (Exception ex)
                        {
                            con.Close();
                            MessageBox.Show(ex.Message);
                        }
                    }
                }
                else
                {
                    throw new Exception("Can't withdraw this much");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
