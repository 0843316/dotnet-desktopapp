﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test3
{
    public class Account
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int AccountNumber { get; set; }
        public double Balance { get; set; }
    }
}
