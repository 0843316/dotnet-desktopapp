﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace Test3
{
    public partial class AdminPanel : Window
    {
        string ConString = ConfigurationManager.ConnectionStrings["BankAccount.Properties.Settings.BankAccountsConnectionString"].ConnectionString;
        string CmdString = string.Empty;

        DataTable dtUsers = new DataTable("Users");

        public AdminPanel()
        {
            InitializeComponent();

            selectAll();
        }


        private void selectAll()
        {
            DataSet ds = new DataSet("Users");
            using (SqlConnection con = new SqlConnection(ConString))
            {
                CmdString = "SELECT [Users].id as 'ID',[Users].Name as 'Name', [Users].AccountNumber as 'Account Number', [Acconts].Balance as 'Balance' FROM [Users] INNER JOIN Acconts ON [Users].AccountNumber = [Acconts].AccountNumber";
                SqlCommand cmd = new SqlCommand(CmdString, con);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(ds,"Users");
                
                dtUsers = ds.Tables["Users"];



                dgUsers.ItemsSource = dtUsers.DefaultView;
                dgUsers.Items.Refresh();
            }
        }
    }
}
