﻿using System.Windows;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System;

namespace Test3
{
    public partial class LoginWindow : Window
    {
        //public Account newStudent = new Account();
        string ConString = ConfigurationManager.ConnectionStrings["BankAccount.Properties.Settings.BankAccountsConnectionString"].ConnectionString;
        string CmdString = string.Empty;

        DataTable dt = new DataTable("Courses");

        public Account loginAccount = new Account();

        public LoginWindow()
        {
            InitializeComponent();
            this.SizeToContent = SizeToContent.Height; // Automatically resize height relative to content.

        }
        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            Login();
        }





        private void Login()
        {


            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConString))
            {
                try
                {
                    
                    CmdString = "SELECT Users.id as 'ID',Users.Name as 'Name',Users.Password as 'Password', Users.AccountNumber as 'Account Number', Acconts.Balance as 'Balance' FROM Users INNER JOIN Acconts ON Users.AccountNumber = Acconts.AccountNumber WHERE Users.Name LIKE \'"+txtUserName.Text+"\';";
                    

                    SqlCommand cmd = new SqlCommand(CmdString, con);
                    SqlDataAdapter sda = new SqlDataAdapter(cmd);
                    sda.Fill(ds, "User");
                    dt = ds.Tables["User"];

                    string password = dt.Rows[0]["Password"].ToString();
                    string name = dt.Rows[0]["Name"].ToString();
                    int id = Int32.Parse(dt.Rows[0]["ID"].ToString());
                    int accountNumber = Int32.Parse(dt.Rows[0]["Account Number"].ToString());
                    double balance = Double.Parse(dt.Rows[0]["Balance"].ToString());



                    if (pwdPassword.Password.Equals(password))
                    {

                        loginAccount = new Account { Id = id, Name = name,AccountNumber=accountNumber,Balance=balance};

                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("wrong password");
                        pwdPassword.Clear();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }
        }
    }
}