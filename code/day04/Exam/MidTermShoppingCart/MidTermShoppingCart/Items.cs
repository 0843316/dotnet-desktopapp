﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MidTermShoppingCart
{
    public class Item
    {
        public string Name { get; set; }
        public double Price { get; set; }

        public Item(string pName, double pPrice)
        {
            Name = pName;
            Price = pPrice;
        }
    }
}