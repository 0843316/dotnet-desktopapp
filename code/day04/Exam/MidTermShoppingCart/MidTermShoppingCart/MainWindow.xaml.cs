﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MidTermShoppingCart
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {


        public MainWindow()
        {
            InitializeComponent();
            ShoppingCart ShoppingCart1 = new ShoppingCart();

        }


        private void AddBtn_Click(object sender, RoutedEventArgs e)
        {
            txtbxTotal.Text = ShoppingCart.AddItem(sender, e, txtbxTotal).Text;
        }

        private void Reset_Click(object sender, RoutedEventArgs e)
        {
            txtbxTotal.Text = ShoppingCart.Reset(txtbxTotal).Text;
        }

        private bool VerifyPayment()
        {
            if ((bool)rbtnCredit.IsChecked)
            {
                if (ShoppingCart.TotalCost < 5)
                {
                    MessageBox.Show("Buy more stuff.");
                    return false;
                }
                else
                {
                    ShoppingCart.PaymentType = "Credit";

                    return true;
                }
            }
            else if ((bool)rbtnCash.IsChecked)
            {
                string txte = txtbxCash.Text;
                if (txtbxCash.Text == null || txtbxCash.Text == "")
                {
                    MessageBox.Show("Y'ain't got no cash.");
                    return false;
                }
                if ((Convert.ToDouble(txte)) < ShoppingCart.TotalCost)
                {
                    MessageBox.Show("Y'ain't got enough dough");
                    return false;
                }
                else
                {
                    ShoppingCart.PaymentType = "Cash";

                    
                    ShoppingCart.AmountReceived = Convert.ToDouble(txtbxCash.Text);
                    ShoppingCart.Change = ShoppingCart.AmountReceived - ShoppingCart.TotalCost;
                    return true;
                }
            }
            else
            {
                MessageBox.Show("Nothing is free, you gotta pay.");
                return false;
            }
        }

        private void Checkout_click(object sender, RoutedEventArgs e)
        {
            if (VerifyPayment())
            {
                ShoppingCart.Checkout();
            }

        }

        private void CreditDebit_Checked(object sender, RoutedEventArgs e)
        {
            txtbxCash.IsEnabled = false;
        }

        private void Cash_Checked(object sender, RoutedEventArgs e)
        {
            txtbxCash.IsEnabled = true;
        }
    }
}
