﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace MidTermShoppingCart
{
    public class ShoppingCart
    {
        public static double TotalCost = 0;
        public static List<Item> ShopItems = new List<Item>();
        public static List<Item> SelectedItems = new List<Item>();
        public static string PaymentType;
        public static double AmountReceived;
        public static double Change;
        public static string Salutation = "Thank you, come again!";

        public ShoppingCart()
        {
            ShopItems.Add(new Item("Apple", 0.5));
            ShopItems.Add(new Item("Bannana", 0.75));
            ShopItems.Add(new Item("Orange", 1));
            ShopItems.Add(new Item("Carrot", 0.7));
            ShopItems.Add(new Item("Potato", 1.25));
            ShopItems.Add(new Item("Spinach", 0.95));
            ShopItems.Add(new Item("Beef", 2.75));
            ShopItems.Add(new Item("Chicken", 4));
            ShopItems.Add(new Item("Pork", 3.5));
            ShopItems.Add(new Item("Milk", 1.75));
            ShopItems.Add(new Item("Cheese", 5.40));
            ShopItems.Add(new Item("Cream", 2.50));
        }

        public static TextBox AddItem(object sender, RoutedEventArgs e,TextBox txtbxTotal)
        {     
            // Get the item that corresponds to the add button
            Item item =
                ShoppingCart.ShopItems.Find(
                    ShopItem =>
                    ShopItem.Name.Equals(
                        ((Button)sender).Name.Substring(4))
                        );

            //TODO : add the item selected to a list of shopping cart
            SelectedItems.Add(item);

            // uptade the Total cost
            ShoppingCart.TotalCost += item.Price;
            double cost = ShoppingCart.TotalCost;
            string strCost = cost.ToString("C");
            txtbxTotal.Text = strCost;
            return txtbxTotal;

            
        }
        public static TextBox Reset(TextBox txtbxTotal)
        {
            ShoppingCart.TotalCost = 0;
            txtbxTotal.Text = 0.ToString("C");
            return txtbxTotal;
        }
        public static string DisplayItems()
        {
            string items = "";
            string displayedString;
            if (SelectedItems.Count == 0)
            {
                return "Buy something";
            }
            else
            {
                var list = SelectedItems;
                var q = from x in list
                        group x by x.Name into g
                        let count = g.Count()
                        
                        orderby count descending
                        select new { Value = g.Key, Count = count };
                foreach (var x in q)

                {

                    items += (x.Count + " " + x.Value + "(s)" + "..." + 
                        (x.Count* ShoppingCart.ShopItems.Find(ShopItem => ShopItem.Name.Equals(x.Value)).Price).ToString("C") + "\n");
                }

                //foreach (Item item in SelectedItems)
                //{
                //    items += item.Name;
                //    items += " ... ";
                //    items += item.Price.ToString("C");
                //    items += "\n";
                //}
            }

            if (PaymentType.Equals("Cash")) {
                displayedString = 
                items + "\n" +
                "Payment Method: " + ShoppingCart.PaymentType + "\n" +
                "Total Cost: " + TotalCost.ToString("C") + "\n" +
                "Amount Received: " + AmountReceived.ToString("C")+"\n" +
                "Change: " + Change.ToString("C") + "\n\n" +
                "Change Breakdown:" + "\n" +
                "20's :" + (int)(Change/20)+ "\n" +
                "10's :" + (int)(Change % 20 / 10) + "\n" +
                "5's :" + (int)(((Change % 20) % 10) /5) + "\n" +
                "2's :" + (int)((((Change % 20) % 10) % 5) /2) + "\n" +
                "1's :" + (int)(((((Change % 20) % 10) % 5) % 2)/1) + "\n" +
                "0.25's :" + (int)((((((Change % 20) % 10) % 5) % 2) % 1)/0.25) + "\n" +
                "0.10's :" + (int)(((((((Change % 20) % 10) % 5) % 2) % 1) % 0.25)/.1) + "\n" +
                "0.05's :" + (int)((((((((Change % 20) % 10) % 5) % 2) % 1) % 0.25) % .1)/.05) + "\n" +
                "0.01's :" + (int)(((((((((Change % 20) % 10) % 5) % 2) % 1) % 0.25) % .1) % .05)/.01) + "\n\n\n\n\n" +
                Salutation;
            }
            else
            {
                displayedString =
                                    items + "\n" +
                "Payment Method: " + ShoppingCart.PaymentType + "\n" +
                "Total Cost: " + TotalCost.ToString("C") + "\n\n\n" +
                Salutation;
            }

            return displayedString;
        }
        public static void Checkout()
        {
            MessageBox.Show(DisplayItems()
                );
        }
    }
}
