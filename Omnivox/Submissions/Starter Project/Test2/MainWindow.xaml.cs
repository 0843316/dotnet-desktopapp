﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace Test2
{
    public partial class MainWindow : Window
    {
        FileManager fileManager = new FileManager();
        List<Student> students = new List<Student>();
        List<Course> courses = new List<Course>();

        public MainWindow()
        {
            InitializeComponent();
            this.SizeToContent = SizeToContent.Height;
        }

        private void btnLoadStudents_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                students = fileManager.loadStudentFile();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnLoadCourses_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                courses = fileManager.loadCourseFile();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnStudentsByCourseWindow_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                new StudentsByCourseWindow(students, courses).ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
