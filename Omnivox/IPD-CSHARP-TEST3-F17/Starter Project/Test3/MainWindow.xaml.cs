﻿using System;
using System.Windows;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.IO;

namespace Test3
{
    public partial class MainWindow : Window
    {
        string ConString = ConfigurationManager.ConnectionStrings["Test3.Properties.Settings.SchoolConnectionString"].ConnectionString;
        string CmdString = string.Empty;

        DataTable dt = new DataTable("Students");

        public MainWindow()
        {
            InitializeComponent();
            this.SizeToContent = SizeToContent.Height; // Automatically resize height relative to content.

            // TODO: When the app starts for the first time, populate the DataGrid.
            selectAll();
        }

        private void btnAddStudent_Click(object sender, RoutedEventArgs e)
        {
            /* TODO:
             * - Show a new AddStudentWindow.
             * - Get the (populated) newStudent object from the new window.
             * - Add the newStudent information to the database (you may want to use the getCourseID() function here).
             */
            AddStudentWindow asw = new AddStudentWindow();
            (asw).ShowDialog();
            Student newStudent = asw.newStudent;


            using (SqlConnection connection = new SqlConnection(ConString))
            {
                CmdString = "INSERT INTO Students (Name, CourseID,FirstCourse) VALUES (\'" + newStudent.Name + "\',\'" + newStudent.CourseID + "\',\'" + newStudent.FirstTime + "\')";
                SqlCommand cmd = new SqlCommand(CmdString, connection);

                try
                {
                    connection.Open();
                    cmd.Connection = connection;
                    cmd.ExecuteNonQuery();
                    //MessageBox.Show("DataBase updated!");
                    connection.Close();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    connection.Close();
                }
            }

            selectAll();


            //string newStudentCourseID = getCourseID(newStudent.CourseID);

        }

        private string getCourseID(string courseName)
        {
            string courseID = "";

            /* TODO:
             * - Return a string that contains the CourseID for the passed in courseName.
             */

            return courseID;
        }

        private void selectAll()
        {
            DataSet ds = new DataSet();
            using (SqlConnection con = new SqlConnection(ConString))
            {
                CmdString = "SELECT Students.id as 'Student ID',Students.Name as 'Student Name',Students.FirstCourse as 'First course?', Courses.Name as 'CourseCourse Name'FROM Students INNER JOIN Courses ON Courses.ID = Students.CourseID";
                SqlCommand cmd = new SqlCommand(CmdString, con);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(ds, "Students");
                dt = ds.Tables["Students"];

                

                dataGridStudents.ItemsSource = dt.DefaultView;
                dataGridStudents.Items.Refresh();
            }
        }

        private void btnSaveStudentList_Click(object sender, RoutedEventArgs e)
        {
            /* TODO:
             * - Save a file named "student_list.txt" with the proper information (refer to the final program I gave you for proper format).
             * - No need for a SaveFileDialog, just save it directly to the current folder.
             */
        }
    }
}
