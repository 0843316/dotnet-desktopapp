﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test3
{
    public class Student
    {
        public string Name { get; set; }
        public string CourseID { get; set; }
        public bool FirstTime { get; set; }
    }
}
