﻿using System.Windows;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace Test3
{
    public partial class AddStudentWindow : Window
    {
        public Student newStudent = new Student();
        string ConString = ConfigurationManager.ConnectionStrings["Test3.Properties.Settings.SchoolConnectionString"].ConnectionString;
        string CmdString = string.Empty;

        DataTable dt = new DataTable("Courses");



        public AddStudentWindow()
        {
            InitializeComponent();
            this.SizeToContent = SizeToContent.Height; // Automatically resize height relative to content.

            populateCoursesComboBox();
        }

        private void btnAddStudent_Click(object sender, RoutedEventArgs e)
        {
            /* TODO:
             * - Populate the newStudent object with the proper information.
             * - Don't forget to validate input (blank textbox and empty combobox selection - refer to the final program I gave you for behaviour). 
             */
            if (!txtStudentName.Text.Equals("") && !cbxCourse.SelectedItem.ToString().Equals(""))
            {
                newStudent.Name = txtStudentName.Text;
                newStudent.FirstTime = (bool)chkFirstCourse.IsChecked;
                newStudent.CourseID = cbxCourse.SelectedItem.ToString();
            }
            else
            {
                MessageBox.Show("Bad stuff...");
            };

            this.Close();

        }

        private void populateCoursesComboBox()
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConString))
            {

                CmdString = "SELECT Name FROM Courses";
                SqlCommand cmd = new SqlCommand(CmdString, con);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(ds, "Courses");
                dt = ds.Tables["Courses"];



                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    cbxCourse.Items.Add(dt.Rows[i]["Name"].ToString());

                }
            }
        }
    }
}