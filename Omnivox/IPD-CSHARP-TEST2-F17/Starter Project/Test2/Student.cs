﻿namespace Test2
{
    public class Student
    {
        public string ID { get; set; }
        public string Name {get; set; }
        public string CourseID { get; set; }
    }
}
