﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;

namespace Test2
{
    public class FileManager
    {
        OpenFileDialog openFileDialog = new OpenFileDialog();
        SaveFileDialog saveFileDialog = new SaveFileDialog();
        public List<Student> loadStudentFile()
        {
            List<Student> students = new List<Student>();
            openFileDialog.ShowDialog();
            Stream s = openFileDialog.OpenFile();
            FileStream fs = (FileStream)s;
            StreamReader sr = new StreamReader(fs);
            while (sr.Peek() >= 0)
            {
                string line = sr.ReadLine();
                string[] valuesArray = line.Split(',');
                students.Add(new Student { ID = valuesArray[0], Name = valuesArray[1], CourseID = valuesArray[2] });
            }
            fs.Close();
            sr.Close();
            s.Close();
            return students;
        }
        public List<Course> loadCourseFile()
        {
            List<Course> courses = new List<Course>();
            openFileDialog.ShowDialog();
            Stream s = openFileDialog.OpenFile();
            FileStream fs = (FileStream)s;
            StreamReader sr = new StreamReader(fs);
            while (sr.Peek() >= 0)
            {
                string line = sr.ReadLine();
                string[] valuesArray = line.Split(',');
                courses.Add(new Course { ID = valuesArray[0], Name = valuesArray[1]});
            }
            return courses;
        }

        public void saveStudentCourseFile(List<Student> students, string course)
        {
            saveFileDialog.ShowDialog();
            if (saveFileDialog.FileName != "")
            {
                FileStream fs = (FileStream)saveFileDialog.OpenFile();
                StreamWriter sw = new StreamWriter(fs);
                string line = "ID,Name,CourseID";
                sw.WriteLine(line);
                foreach (Student x in students)
                {
                    line = x.ID;
                    line += ",";
                    line += x.Name;
                    line += ",";
                    line += x.CourseID;
                    sw.WriteLine(line);
                }
                sw.Close();
                fs.Close();
            }


            
        }
    }
}
