﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Test2
{
    public partial class StudentsByCourseWindow : Window
    {
        List<Student> students;
        List<Student> filteredStudents = new List<Student>();
        string selectedCourse;

        public StudentsByCourseWindow(List<Student> students, List<Course> pcourses)
        {
            InitializeComponent();
            this.students = students;
            lbxCourses.ItemsSource = pcourses;
            this.SizeToContent = SizeToContent.Height; // Automatically sizes the height of the window.
        }
        private void lbxCourses_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbxCourses.SelectedItem != null)
            {
                selectedCourse = (lbxCourses.SelectedItem as Course).ID;
                filterStudents(selectedCourse);
            }
        }
        private void filterStudents(string courseID)
        {
            filteredStudents.Clear();
            foreach (Student x in students)
            {
                if (x.CourseID.Equals(courseID))
                {
                    filteredStudents.Add(x);
                }
            }
            dataGridStudents.ItemsSource = filteredStudents;
            dataGridStudents.Items.Refresh();
        }
        private void btnExportCurrentClassList_Click(object sender, RoutedEventArgs e)
        {
            FileManager fm = new FileManager();
            fm.saveStudentCourseFile(filteredStudents, selectedCourse);
        }
    }
}
